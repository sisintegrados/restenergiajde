package com.sisintegrados.unopetrol.restenergiajde;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestEnergiaJdeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestEnergiaJdeApplication.class, args);
	}

}
